// Karma configuration
    var path = require('path');
    var cwd = process.cwd();
   
    module.exports = function(config) {
      config.set({
        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '.',
   
        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['jasmine'],
    
        // list of files / patterns to load in the browser
        files: [
          { pattern: 'spec-bundle.js', watched: false }
        ],
   
        // list of files to exclude
        exclude: [
        ],
   
        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
          'spec-bundle.js': ['webpack']
        },
   
        webpack: {
          resolve: {
            root: [path.resolve(cwd)],
            modulesDirectories: ['node_modules', 'app', 'app/ts', 'test', '.'],
            extensions: ['', '.ts', '.js', '.css'],
            alias: {
              'app': 'app'
            }
          },
          module: {
            loaders: [
              { test: /\.ts$/, loader: 'ts-loader', exclude: [/node_modules/]}
            ]
          },
          devtool: 'inline-source-map',
          stats: {
            colors: true,
            reasons: true
          },
          watch: true,
          debug: true
        },
   
        webpackServer: {
          noInfo: true
        },  
   
        reporters: ['dots'],       
        port: 9830,
        colors: true,
   
    
        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,
   
    
        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,
   
    
        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['PhantomJS'],
   
    
        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: false
      })
    }
 