webpackJsonp([0],{

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var platform_browser_dynamic_1 = __webpack_require__(1);
	var user_app_component_1 = __webpack_require__(280);
	platform_browser_dynamic_1.bootstrap(user_app_component_1.UserAppComponent, []);


/***/ },

/***/ 280:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
	    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
	    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
	    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
	    return c > 3 && r && Object.defineProperty(target, key, r), r;
	};
	var __metadata = (this && this.__metadata) || function (k, v) {
	    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
	};
	var core_1 = __webpack_require__(7);
	var common_1 = __webpack_require__(181);
	var user_list_component_1 = __webpack_require__(281);
	var users_service_1 = __webpack_require__(282);
	var UserAppComponent = (function () {
	    function UserAppComponent(usersService) {
	        var _this = this;
	        this.usersService = usersService;
	        this.users = [];
	        this.filter = new common_1.Control();
	        usersService.subscribe(function (users) {
	            _this.users = users;
	        });
	        usersService.init();
	    }
	    UserAppComponent.prototype.ngOnInit = function () {
	        var _this = this;
	        this.filter.valueChanges.debounceTime(500)
	            .subscribe(function (nameStartsWith) { return _this.usersService.filterUsers(nameStartsWith); });
	    };
	    UserAppComponent = __decorate([
	        core_1.Component({
	            selector: "user-app",
	            directives: [user_list_component_1.UserListComponent],
	            providers: [users_service_1.UsersService],
	            template: "\n        <div class=\"container\">\n            <div class=\"row\">\n                <div class=\"col-md-3\"></div>\n                <div class=\"col-md-6\">\n                    <h1>People search</h1>\n                    <div class=\"row\">\n                        <form class=\"col-xs-12\">\n                            <input type=\"text\" \n                                   class=\"form-control\" \n                                   placeholder=\"filter users\" \n                                   [ngFormControl]=\"filter\" \n                                   autofocus \n                                   autocomplete=\"off\">\n                        </form>                                                \n                    </div>\n                    <br>\n                    <user-list [users]=\"users\"></user-list>\n                </div>\n                <div class=\"col-md-3\"></div>\n            </div>\n            <div tabindex=\"0\" (focus)=\"filterInput.focus()\"></div>\n        </div>\n    ",
	        }), 
	        __metadata('design:paramtypes', [users_service_1.UsersService])
	    ], UserAppComponent);
	    return UserAppComponent;
	}());
	exports.UserAppComponent = UserAppComponent;


/***/ },

/***/ 281:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
	    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
	    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
	    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
	    return c > 3 && r && Object.defineProperty(target, key, r), r;
	};
	var __metadata = (this && this.__metadata) || function (k, v) {
	    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
	};
	var core_1 = __webpack_require__(7);
	var UserListComponent = (function () {
	    function UserListComponent() {
	        this.users = [];
	    }
	    UserListComponent.prototype.createImageUrl = function (imageName) {
	        return "images/" + imageName;
	    };
	    __decorate([
	        core_1.Input(), 
	        __metadata('design:type', Array)
	    ], UserListComponent.prototype, "users", void 0);
	    UserListComponent = __decorate([
	        core_1.Component({
	            selector: "user-list",
	            styles: ["\n        .img-responsive {\n            max-height: 180px;\n            height: 180px;\n            max-width: 150px;\n            margin: 0 auto;\n        }\n        \n        .user-name {\n            text-align: center;\n        }\n    "],
	            template: "\n        <div class=\"row\">\n            <div class=\"col-lg-4 col-md-6 col-sm-6 col-xs-6 row-eq-height user\" *ngFor=\"let user of users\">\n                <img [src]=\"createImageUrl(user.image)\" class=\"img-responsive hidden-xs\" />\n                <a href=\"#\"><h3 class=\"text-center user-name\">{{ user.name }}</h3></a>\n            </div>\n        </div>\n    "
	        }), 
	        __metadata('design:paramtypes', [])
	    ], UserListComponent);
	    return UserListComponent;
	}());
	exports.UserListComponent = UserListComponent;


/***/ },

/***/ 282:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
	    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
	    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
	    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
	    return c > 3 && r && Object.defineProperty(target, key, r), r;
	};
	var __metadata = (this && this.__metadata) || function (k, v) {
	    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
	};
	var core_1 = __webpack_require__(7);
	var rxjs_1 = __webpack_require__(283);
	var user_1 = __webpack_require__(540);
	var UsersService = (function () {
	    function UsersService() {
	        this._users$ = new rxjs_1.BehaviorSubject([]);
	    }
	    UsersService.prototype.init = function () {
	        this._users$.next(UsersService.users);
	    };
	    UsersService.prototype.filterUsers = function (filter) {
	        if (!filter) {
	            this._users$.next(UsersService.users);
	            return;
	        }
	        this._users$.next(UsersService.users.filter(function (user) { return user.name.toUpperCase().startsWith(filter.toUpperCase()); }));
	    };
	    UsersService.prototype.subscribe = function (next) {
	        return this._users$.subscribe(next);
	    };
	    UsersService.users = [
	        new user_1.User("Sean", "sean.png"),
	        new user_1.User("Yaw", "yaw.png"),
	        new user_1.User("Lucy", "lucy.png"),
	        new user_1.User("Eric", "eric.png"),
	        new user_1.User("Rory", "rory.png"),
	        new user_1.User("Erica", "eric.png"),
	        new user_1.User("Edwin", "sean.png"),
	        new user_1.User("Hayley", "hayley.png")
	    ];
	    UsersService = __decorate([
	        core_1.Injectable(), 
	        __metadata('design:paramtypes', [])
	    ], UsersService);
	    return UsersService;
	}());
	exports.UsersService = UsersService;


/***/ },

/***/ 540:
/***/ function(module, exports) {

	"use strict";
	var User = (function () {
	    function User(name, image) {
	        this.name = name;
	        this.image = image;
	    }
	    return User;
	}());
	exports.User = User;


/***/ }

});
//# sourceMappingURL=app-bundle.js.map