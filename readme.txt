Execution steps:
    1. npm install
    2. npm run build (this step will create a 'dist' folder with the deployable artifacts)

As an alternative:
    1. npm install
    2. npm start (this step will run a local server serving the page)
    3. open the following address in a browser: http://localhost:8080/webpack-dev-server/

I am also including the 'dist' folder with the content.

Unit tests are in the same folder as a test target, e.g. user-list.component.ts and user-list.component.spec.ts
    1. npm test 

Thanks,
Aleksey

 