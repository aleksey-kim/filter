var path = require('path');
var webpack = require('webpack');
var CommonsChunkPlugin = webpack.optimize.CommonsChunkPlugin;
var CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: {
        app: './app/ts/main',
        vendor: './app/ts/vendor',
        polyfills: './app/ts/polyfills'        
    },
    output: {
        path: './dist',
        filename: '[name]-bundle.js'
    },
    resolve: {
        extensions: ['', '.ts', '.js']
    },
    devServer: {
        contentBase: 'dist/'    
    },
    devtool: 'source-map',
    watch: false,
    module: {
        loaders: [
            {
                test: /\.ts$/,
                loader: 'ts'
            }
        ]
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: ['app', 'vendor', 'polyfills']
        }),
        
        new CopyWebpackPlugin([
            { from: './app/index.html', to: 'index.html'},
            { from: './app/assets/images', to: 'images'}            
        ])        
    ]
};