import { Component, Input } from "@angular/core";
import { User } from "../models/user";

@Component({
    selector: "user-list",
    styles: [`
        .img-responsive {
            max-height: 180px;
            height: 180px;
            max-width: 150px;
            margin: 0 auto;
        }
        
        .user-name {
            text-align: center;
        }
    `],    

    template: `
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 row-eq-height user" *ngFor="let user of users">
                <img [src]="createImageUrl(user.image)" class="img-responsive hidden-xs" />
                <a href="#"><h3 class="text-center user-name">{{ user.name }}</h3></a>
            </div>
        </div>
    `
})
export class UserListComponent {
    @Input()
    users: User[] = [];
    
    createImageUrl(imageName: string): string {
        return `images/${imageName}`;
    }
}
