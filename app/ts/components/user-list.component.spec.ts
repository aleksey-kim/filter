import { describe, it, expect, beforeEachProviders, beforeEach, inject, async } from "@angular/core/testing";
import { TestComponentBuilder, ComponentFixture } from "@angular/compiler/testing";

import { UserListComponent } from "./user-list.component";
import { User } from "../models/user";

describe("UserListComponent", () => {
    
    let builder: TestComponentBuilder;
    
    beforeEachProviders(() => [TestComponentBuilder]);
    
    beforeEach(inject([TestComponentBuilder], (tcb) => {
        builder = tcb;
    }));   
   
    it("should have an empty user list by default", () => {
        let component = new UserListComponent();
        
        expect(component.users).toBeTruthy();
        expect(component.users.length).toBe(0);    
    }); 
    
    it("should render 'div.user' for each user in the user list", async(() => {
        builder.createAsync(UserListComponent)
               .then((fixture: ComponentFixture<UserListComponent>) => {
                   let component: UserListComponent = fixture.componentInstance;
                   let element = fixture.nativeElement;
                    
                   let users: User[] = [
                       new User("test1", "test1.png"),
                       new User("test2", "test2.png")
                   ];
                    
                   component.users = users;
                   fixture.detectChanges();
                    
                   let userDivs = element.querySelectorAll("div.user");
                    
                   expect(userDivs.length).toBe(2);                   
                   
                   expectUserImageIsSetCorrectly(userDivs[0], users[0]);
                   expectUserImageIsSetCorrectly(userDivs[1], users[1]);
                   
                   expectUserNameIsSetCorrectly(userDivs[0], users[0]);
                   expectUserNameIsSetCorrectly(userDivs[1], users[1]);
                    
                   function expectUserNameIsSetCorrectly(userElement: any, user: User) {
                       expect(userElement.querySelector(".user-name").innerText).toBe(user.name);                        
                   }
                   
                   function expectUserImageIsSetCorrectly(userElement: any, user: User) {
                       expect(userElement.querySelector("img").src.includes(user.image)).toBe(true);                        
                   }                   
               });
    }));
});
