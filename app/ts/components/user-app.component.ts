import { Component } from "@angular/core";
import { Control } from "@angular/common";

import { UserListComponent } from "./user-list.component";
import { User } from "../models/user";
import { UsersService } from "../services/users.service";

@Component({
    selector: "user-app",
    directives: [UserListComponent],
    providers: [UsersService],
    template: `
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <h1>People search</h1>
                    <div class="row">
                        <form class="col-xs-12">
                            <input type="text" 
                                   class="form-control" 
                                   placeholder="filter users" 
                                   [ngFormControl]="filter" 
                                   autofocus 
                                   autocomplete="off">
                        </form>                                                
                    </div>
                    <br>
                    <user-list [users]="users"></user-list>
                </div>
                <div class="col-md-3"></div>
            </div>
            <div tabindex="0" (focus)="filterInput.focus()"></div>
        </div>
    `,
})
export class UserAppComponent {
    users: User[] = [];
    filter: Control = new Control();

    constructor(private usersService: UsersService) {
        usersService.subscribe((users: User[]) => {
            this.users = users;
        });

        usersService.init();
    }

    ngOnInit() {
        this.filter.valueChanges.debounceTime(500)
                                .subscribe(nameStartsWith => this.usersService.filterUsers(nameStartsWith));
    }
}
