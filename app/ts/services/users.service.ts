import { Injectable } from "@angular/core";
import { Subject, BehaviorSubject, Subscription } from "rxjs";

import { User } from "../models/user";

@Injectable()
export class UsersService {
    static users: User[] = [
        new User("Sean", "sean.png"),    
        new User("Yaw", "yaw.png"),    
        new User("Lucy", "lucy.png"),    
        new User("Eric", "eric.png"),    
        new User("Rory", "rory.png"),    
        new User("Erica", "eric.png"),    
        new User("Edwin", "sean.png"),    
        new User("Hayley", "hayley.png")    
    ];
    
    private _users$: Subject<User[]> = new BehaviorSubject<User[]>([]);    
    
    public init(): void {        
        this._users$.next(UsersService.users);        
    }
    
    public filterUsers(filter: string): void {
        if (!filter) {
            this._users$.next(UsersService.users);
            return;
        }
        
        this._users$.next(UsersService.users.filter(user => user.name.toUpperCase().startsWith(filter.toUpperCase())));
    }

    public subscribe(next: (users: User[]) => void ): Subscription {
        return this._users$.subscribe(next);
    }
}
